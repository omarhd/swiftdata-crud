//
//  ContentView.swift
//  Swift Data Tut
//
//  Created by Omar Abdulrahman on 25/10/2023.
//

import SwiftUI
import SwiftData

struct ContentView: View {
    
    @Environment(\.modelContext) var context

    @State private var isShowingItemSheet = false
    @State private var productToEdit: Product?
    
    @Query(sort: \Product.date) var products: [Product]
    
    var body: some View {
        NavigationStack {
            List {
                ForEach(products) { product in
                    ProductCell(product: product)
                        .onTapGesture {
                            productToEdit = product
                        }
                }
                .onDelete(perform: { indexSet in
                    for index in indexSet {
                        context.delete(products[index])
                    }
                })
            }
            
            .navigationTitle("Products")
            .navigationBarTitleDisplayMode(.large)
            .sheet(isPresented: $isShowingItemSheet) { AddProductSheet() }
            .sheet(item: $productToEdit) { product in UpdateProduct(product: product)}
            .toolbar {
                if !products.isEmpty {
                    Button("Add Product", systemImage: "plus") {
                     isShowingItemSheet = true
                    }
                }
            }
            .overlay {
                if products.isEmpty {
                    ContentUnavailableView(label: {
                        Label("No Products", systemImage: "list.bullet.rectangle.portrait")
                    }, description: {
                        Text("Start adding Product to see your list.")
                    }, actions: {
                        Button("Add Product") { isShowingItemSheet = true }
                    })
                    .offset(y: -60)
                }
            }
        }
    }
}

#Preview {
    ContentView()
}
