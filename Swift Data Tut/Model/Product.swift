//
//  Product.swift
//  Swift Data Tut
//
//  Created by Omar Abdulrahman on 25/10/2023.
//

import Foundation
import SwiftData

@Model
class Product {
    var name: String
    var date: Date
    var price: Double
    
    init(name: String, date: Date, price: Double) {
        self.name = name
        self.date = date
        self.price = price
    }
}
