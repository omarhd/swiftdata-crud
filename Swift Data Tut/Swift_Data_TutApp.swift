//
//  Swift_Data_TutApp.swift
//  Swift Data Tut
//
//  Created by Omar Abdulrahman on 25/10/2023.
//

import SwiftUI
import SwiftData

@main
struct Swift_Data_TutApp: App {
    
    let container: ModelContainer = {
        let scheme = Schema([Product.self])
        let container = try! ModelContainer(for: scheme, configurations: [])
        return container
    }()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        .modelContainer(for: [Product.self])
    }
}
