//
//  UpdateProduct.swift
//  Swift Data Tut
//
//  Created by Omar Abdulrahman on 26/10/2023.
//

import SwiftUI

struct UpdateProduct: View {
    
    @Environment(\.dismiss) private var dismiss
    
    @Bindable var product: Product
    
    var body: some View {
        NavigationStack {
            Form {
                TextField("Expense Name", text: $product.name)
                DatePicker("Date", selection: $product.date, displayedComponents: .date)
                TextField("Price", value: $product.price, format: .currency(code: "USD"))
                    .keyboardType(.decimalPad)
            }

            .navigationTitle("Edit Product")
            .navigationBarTitleDisplayMode(.large)
            .toolbar {
                ToolbarItemGroup(placement: .topBarLeading) {
                    Button("Cancel") { dismiss() }
                    }
                    ToolbarItemGroup(placement: .topBarTrailing) {
                        Button("Done") { dismiss()
                    }
                }
            }
        }
    }
}

#Preview {
    AddProductSheet()
}
